// RackN Provision Plugins
// Copyright 2018, RackN
// License: RackN Limited Use

package main

//go:generate drbundler content content.go
//go:generate drbundler content content.yaml
//go:generate sh -c "drpcli contents document content.yaml > training.rst"
//go:generate rm content.yaml

import (
	"fmt"
	"os"
	"time"

	"github.com/digitalrebar/logger"
	"github.com/digitalrebar/provision/api"
	"github.com/digitalrebar/provision/models"
	"github.com/digitalrebar/provision/plugin"
	"github.com/rackn/provision-plugins"
	"github.com/rackn/provision-plugins/utils"
)

// ==== PRIMARY ENTRY POINT FOR PLUGIN ====

// def information is passed to DRP during plugin registration
var (
	version = provision_plugins.RS_VERSION
	def     = models.PluginProvider{
		Name:          "training",
		Version:       version,
		PluginVersion: 2,
		HasPublish:    false,
		Content: contentYamlString,
	}
)

// Plugin is the overall data holder for the plugin
// If you defined extra operational values or params, they are typically included here
type Plugin struct {
	drpClient *api.Client
	name      string
}

// Config handles the configuration call from the DRP Endpoint
func (p *Plugin) Config(l logger.Logger, session *api.Client, config map[string]interface{}) *models.Error {
	p.drpClient = session
	if name, err := utils.ValidateStringValue("Name", config["Name"]); err != nil {
		p.name = "unknown"
	} else {
		p.name = name
	}
	utils.SetErrorName(p.name)

	if _, verr := utils.ValidateLicense(session, &def); verr != nil {
		go func() {
			time.Sleep(3 * time.Second)
			os.Exit(1)
		}()
		return verr
	}
	utils.RevalidateLicense(session, &def, 1*time.Minute, nil)

	return nil
}

// Action handles the action call from the DRP Endpoint
// using ma.Command, all registered actions should be handled
// reminder when validating params:
//   DRP will pass in required machine params if they exist in hierarchy
func (p *Plugin) Action(l logger.Logger, ma *models.Action) (answer interface{}, err *models.Error) {

	switch ma.Command {
	default:
		err = utils.MakeError(404, fmt.Sprintf("Unknown command: %s", ma.Command))
	}

	return
}

// validate allows the plugin to confirm that it can run in the environment
// typically, we also confirm licenses in this routine
func (p *Plugin) Validate(l logger.Logger, session *api.Client) (interface{}, *models.Error) {
	utils.SetErrorName(def.Name)
	if _, err := utils.ValidateLicense(session, &def); err != nil {
		return nil, err
	}
	return def, nil
}

// main is the entry point for the plugin code
// the InitApp routine should reflect the name and purpose of the plugin
func main() {
	plugin.InitApp("training", "This is an example plugin for Training", version, &def, &Plugin{})
	err := plugin.App.Execute()
	if err != nil {
		os.Exit(1)
	}
}
