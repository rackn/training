// RackN Provision Plugins
// Copyright 2018, RackN
// License: RackN Limited Use

package main

import (
	utils2 "github.com/VictorLowther/jsonpatch2/utils"
	"github.com/digitalrebar/logger"
	//"github.com/digitalrebar/provision/api"
	"github.com/digitalrebar/provision/models"
	"github.com/rackn/provision-plugins/utils"
)

// This is general behavior for the plugin, it is not directly called from the actions
// Set the machine's training to traing
// Set the machine's position to position
// If position = 0, remove it.  If position < 0 then remove machine and position
func (p *Plugin) trainingMachine(l logger.Logger, ma *models.Action, training string, position int) (answer interface{}, err *models.Error) {
	// get machine referenced by action and clone into new model
	machine := &models.Machine{}
	machine.Fill()
	if rerr := utils2.Remarshal(ma.Model, &machine); rerr != nil {
		return "", utils.ConvertError(400, rerr)
	}
	trainingMachine := models.Clone(machine).(*models.Machine)
	l.Debugf("Training operation on machine %s to training %s at position %d.\n", machine.UUID, training, position)
	// set the params on the copy
	trainingMachine.Params["training/name"] = training
	trainingMachine.Params["training/position"] = position
	// special actions based on position given
	// 0 = no position
	if position <= 0 {
		delete(trainingMachine.Params, "training/position")
		l.Debugf("Training operation on machine %s remove position.\n", machine.UUID)
	}
	// -1 = remove rack entry too
	if position < 0 {
		delete(trainingMachine.Params, "training/name")
		l.Debugf("Training operation on machine %s remove training.\n", machine.UUID)
	}
	// patch model 
	if _, perr := p.drpClient.PatchTo(machine, trainingMachine); perr != nil {
		return "", utils.ConvertError(400, perr)
	}

	answer = "success"
	return
}

// Add Machine into Training
// Action Target Model: Machine
// Param training already validated
func (p *Plugin) addToTraining(l logger.Logger, ma *models.Action, training string) (answer interface{}, err *models.Error) {
	l.Debugf("Starting insert to training %s\n", training)

	// fill the params passed with the call (provides defaults)
	position := utils.GetParamOrInt(ma.Params, "training/position", 0)
	answer, err = p.trainingMachine(l, ma, training, position)

	l.Debugf("Finished insert of machine to training %s at position %d\n", training, position)
	return
}

// Remove Machine from Training
// Action Target Model: Machine
func (p *Plugin) removeFromTraining(l logger.Logger, ma *models.Action) (answer interface{}, err *models.Error) {
	l.Debugf("Starting eject from training\n")

	answer, err = p.trainingMachine(l, ma, "", -1)

	l.Debugf("Finished untraining of machine\n")
	return
}