#!/usr/bin/env bash
# RackN Provision Plugins
# Copyright 2018, RackN
# License: RackN Limited Use


#
# This is a test of the pool system.  It does not require machines or a working system.
# It does require the RS_ENDPOINT and RS_KEY or RS_TOKEN are set.
#

FIX_IT=0
if [[ "$1" == "--fix" ]] ; then
    FIX_IT=1
fi

src="test-data"

runTest() {
    local name=$1
    local cmd=$2

    mkdir -p "$src/$name"
    (
        cd "$src/$name"
        rm -f untouched
        eval $cmd > stdout.actual 2> stderr.actual
    )
    if validateTest "$src/$name" "$cmd" ; then
        echo "SUCCESS: $name"
    else
        echo "Test Name: $name"
        echo "Command: $cmd"
        echo "FAIL: $name"
    fi
}

edit() {
    local file=$1
    if [[ $EDITOR ]] ; then
        cat "$file".actual
        ${EDITOR} "$file".expect
    else
        echo "Actual file: $file.actual"
        cat "$file".actual
        echo "Expect file: $file.expect"
        cat "$file".expect
        read -p "Fix $file.actual by editing $file.expect.  Done?" ans
    fi
}

markTests() {
    readarray -d $'\0' dirs < <(find "$src" -type d -print0 |sort -z)

    for dir in "${dirs[@]}"; do
        if [[ $dir == $src ]] ; then
            continue
        fi
        [[ -f $dir/stdout.expect || -f $dir/stderr.expect ]] && touch "$dir/untouched"
    done
}

validateTest() {
    local dir=$1
    local cmd=$2
    local rc=0

    for ft in stderr stdout; do
        diff -u "$dir/$ft".expect "$dir/$ft".actual 2>/dev/null && continue
        rc=1
        if [[ $FIX_IT -eq 0 ]] ; then
            continue
        fi
        echo "Name: $dir"
        echo "Command: $cmd"
        echo "----"
        cat "$dir/$ft".actual
        echo "----"
        read -p "Move $ft.actual to $ft.expect? (y/e/n)" ans
        case $ans in
            y) mv "$dir/$ft".actual "$dir/$ft".expect;;
            e) edit "$dir/$ft";;
        esac
    done

    return $rc
}

removeOldTests() {
    readarray -d $'\0' dirs < <(find "$src" -type d -print0 |sort -z)

    for dir in "${dirs[@]}"; do
        if [[ $dir == $src ]] ; then
            continue
        fi
        [[ -f $dir/untouched ]] && rm -rf $dir
    done
}

# Mark tests that can be cleaned up
markTests

# Setup
drpcli machines create '{"Name":"training.plugin.test"}' 2>&1 >/dev/null

# Run tests - param does not exist
runTest Cli0001 "drpcli plugins show training"
runTest Cli0002 "drpcli machines action Name:training.plugin.test addToTraining"
runTest Cli0003 "drpcli machines action Name:training.plugin.test removeFromTraining"
runTest Cli0004 "drpcli machines action Name:training.plugin.test greatTraining"

# Teardown
drpcli machines destroy Name:"training.plugin.test" >/dev/null

# Remove old tests
removeOldTests

