# RackN plugin training examples

The following material is confidential and licensed for RackN customers.

Use via permission only.  No reproduction rights.

Lesson 1
--------

Shows how to create and test a working stub plugin!

https://youtu.be/j6PCZoJKVQI [PERMISSION REQUIRED]

Lesson 2
--------

Shows how to add and verify actions to a plugin

https://youtu.be/0dL1HQQEwLE [PERMISSION REQUIRED]